package com.izlai.bartr.feature.login.presenter;

import android.content.Context;

import com.izlai.bartr.feature.login.view.LoginView;

/**
 * Copyright © 2016 bestworkvn.
 * Created by thaivuvo on 24/04/2018.
 */

public class LoginPresenterImp implements LoginPresenter {

    private Context mContext;
    private LoginView view;


    public LoginPresenterImp(Context context, LoginView view) {
        this.view = view;
        this.mContext = context;
    }

    @Override
    public void loginProcess(String user, String pass, boolean remember) {
        //call API here
    }
}
