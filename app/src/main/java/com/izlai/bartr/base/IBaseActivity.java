package com.izlai.bartr.base;

import com.izlai.bartr.view.DialogMessage;

/**
 * Created by thaivuvo on 2018/04/24
 */

public interface IBaseActivity {

    DialogMessage showDialogSuccess(String msg);

    void forceDismissLoading();

    DialogMessage showDialogError(String msg);

    void dismissLoading();

    void showLoading();

    void showDialogPermission(String msg);

}
