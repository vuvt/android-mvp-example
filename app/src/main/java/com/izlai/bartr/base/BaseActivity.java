package com.izlai.bartr.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.izlai.bartr.BartrApplication;
import com.izlai.bartr.view.DialogMessage;
import com.izlai.bartr.view.LoadingProgress;

/**
 * Created by thaivuvo on 2018/04/24
 */

public class BaseActivity extends AppCompatActivity implements IBaseActivity {

    private LoadingProgress loadingProgress;

    public byte hasUpdate = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingProgress = new LoadingProgress(this);
    }

    public void showLoading() {
        if (!loadingProgress.isShowing())
            loadingProgress.show();
    }

    @Override
    protected void onDestroy() {
        try {
            if (loadingProgress != null && loadingProgress.isShowing()) {
                loadingProgress.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public void dismissLoading() {
        if (!isFinishing()) {
            if (loadingProgress.isShowing())
                loadingProgress.dismiss();
        }
    }

    public DialogMessage showDialogError(String msg) {
        DialogMessage dialogMessage = new DialogMessage(this, msg);
        dialogMessage.show();
        return dialogMessage;
    }

    public void forceDismissLoading() {
        loadingProgress.forceDismiss();
    }

    public DialogMessage showDialogSuccess(String msg) {
        DialogMessage dialogMessage = new DialogMessage(this, msg);
        dialogMessage.show();
        return dialogMessage;
    }

    @Override
    protected void onResume() {
        super.onResume();
        hasUpdate = 0;
    }

    @Override
    public void startActivity(Intent intent) {
        if (hasUpdate == 1) return;
        super.startActivity(intent);
        hasUpdate = 2;
    }

    @Override
    public void showDialogPermission(String msg) {
        showDialogSuccess(msg);
    }
}
