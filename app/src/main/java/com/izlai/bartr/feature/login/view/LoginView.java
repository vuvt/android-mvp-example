package com.izlai.bartr.feature.login.view;

/**
 * Copyright © 2016 bestworkvn.
 * Created by thaivuvo on 24/04/2018.
 */

public interface LoginView {
    void showLoading();
    void dismissLoading();
    void errorValidLogin(int type);
    void errorLoginRequest(String msg);
}
