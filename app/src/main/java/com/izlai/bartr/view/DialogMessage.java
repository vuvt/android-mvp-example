package com.izlai.bartr.view;

import android.app.Dialog;
import android.content.Context;
import android.text.Spanned;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.izlai.bartr.R;

/**
 * Created by thaivuvo on 2018/04/24
 */

public class DialogMessage extends Dialog {

    OnButtonClick onButtonClick;
    private int countLoading = 0;

    public DialogMessage(Context context, String msg) {
        super(context);

        initLoadingProgress(context);
        ((TextView) findViewById(R.id.txtMsg)).setText(msg != null ? msg : "");
    }

    public DialogMessage(Context context, Spanned msg) {
        super(context);

        initLoadingProgress(context);
        ((TextView) findViewById(R.id.txtMsg)).setText(msg);
    }

    /*public void setTextButtonOK(String message) {
        ((Button) findViewById(R.id.btnOk)).setText(message);
    }*/
    private void initLoadingProgress(Context context) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dialog_message);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(this.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        this.getWindow().setAttributes(lp);
        this.setCancelable(false);
        this.setCanceledOnTouchOutside(false);
        /*this.findViewById(R.id.btnOk).setOnClickListener(view -> {
            dismiss();
            if(onButtonClick != null){
                onButtonClick.onClick();
            }
        });*/
    }

    public DialogMessage showMsg() {
        if (countLoading == 0) {
            super.show();
        }
        countLoading++;
        return this;
    }

    public void dismissMsg() {
        countLoading--;
        if (countLoading > 0) return;
        super.dismiss();
    }

    public void forceDismissMsg() {
        countLoading = 0;
        super.dismiss();
    }

    public void setOnButtonClick(OnButtonClick onButtonClick) {
        this.onButtonClick = onButtonClick;
    }

    public interface OnButtonClick {
        void onClick();
    }
}
