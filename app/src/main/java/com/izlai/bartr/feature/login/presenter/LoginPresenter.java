package com.izlai.bartr.feature.login.presenter;

/**
 * Copyright © 2016 bestworkvn.
 * Created by thaivuvo on 24/04/2018.
 */

public interface LoginPresenter {
    void loginProcess(String user, String pass, boolean remember);
}
