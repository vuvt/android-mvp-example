package com.izlai.bartr.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import com.izlai.bartr.R;

/**
 * Created by thaivuvo on 2018/04/24
 */

public class LoadingProgress extends Dialog {

    private int countLoading = 0;

    public LoadingProgress(Context context) {
        super(context);

        initLoadingProgress(context);
    }

    private void initLoadingProgress(Context context) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.loading_progress);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.setCancelable(false);
        this.setCanceledOnTouchOutside(false);
    }

    @Override
    public void show() {
        if (countLoading == 0) {
            super.show();
        }
        countLoading++;
    }

    @Override
    public void dismiss() {
        countLoading--;
        if (countLoading > 0 || !super.isShowing()) return;
        super.dismiss();
    }

    public void forceDismiss() {
        countLoading = 0;
        super.dismiss();
    }
}
