package com.izlai.bartr.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.izlai.bartr.view.DialogMessage;

/**
 * Created by thaivuvo on 2018/04/24
 */

@SuppressLint("ValidFragment")
public class BaseFragment extends Fragment {
    public Context mContext;

    public BaseFragment() {
    }

    public BaseFragment(Context context) {
        this.mContext = context;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void showLoading() {
        if (mContext != null && getActivity() != null && !getActivity().isFinishing()) {
            ((IBaseActivity) mContext).showLoading();
        }
    }

    public void dismissLoading() {
        if (mContext != null && getActivity() != null && !getActivity().isFinishing()) {
            ((IBaseActivity) mContext).dismissLoading();
        }
    }

    public DialogMessage showDialogError(String msg) {
        if (mContext == null || getActivity() == null || getActivity().isFinishing()) {
            return null;
        }
        return ((IBaseActivity) mContext).showDialogError(msg);
    }

    public void forceDismissLoading() {
        if (mContext == null) return;
        ((IBaseActivity) mContext).forceDismissLoading();
    }

    public DialogMessage showDialogSuccess(String msg) {
        if (mContext == null || getActivity() == null || getActivity().isFinishing()) {
            return null;
        }
        return ((IBaseActivity) mContext).showDialogSuccess(msg);
    }
}
